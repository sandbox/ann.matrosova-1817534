<?php

/**
 * @file
 * Functionality and helper functions for chosenselect module.
 */

/**
 * Helper function for chosen_field_widget_settings_form().
 */
function _chosenselect_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($instance['widget']['type'] == 'chosen_term_widget' || $instance['widget']['type'] == 'chosen_select_widget') {
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#description' => t('Form element width in pixels.'),
      '#default_value' => $settings['width'],
      '#size' => 20,
    );
    $form['block'] = array(
      '#type' => 'checkbox',
      '#title' => t('One item per row'),
      '#description' => t('Display items in a block manner - one item per row.'),
      '#default_value' => $settings['block'],
    );
    $form['placeholder'] = array(
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#description' => t('Text used for an element placeholder.'),
      '#default_value' => $settings['placeholder'],
      '#size' => 20,
    );
  }

  if ($instance['widget']['type'] == 'chosen_term_widget') {
    $form['sortable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Sortable'),
      '#description' => t('If checked, it will be possible to reorder chosen items.'),
      '#default_value' => $settings['sortable'],
    );
    $form['free_tagging'] = array(
      '#type' => 'checkbox',
      '#title' => t('Free tagging'),
      '#description' => t('Enables free tagging mode which allows to add non-existent tags.'),
      '#default_value' => $settings['free_tagging'],
    );
    $form['add_new_tag'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add new only if empty'),
      '#description' => t('If checked, adding a new tag will only be allowed when the AJAX returns no other tags.'),
      '#default_value' => $settings['add_new_tag'],
      '#states' => array(
        'visible' => array(
          '#edit-instance-widget-settings-free-tagging' => array('checked' => TRUE),
        ),
        'invisible' => array(
          '#edit-instance-widget-settings-free-tagging' => array('checked' => FALSE),
        ),
      ),

    );
    $form['min_term_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimum term length'),
      '#description' => t('Specify the minimum amount of characters a user need to enter before the autocompletion AJAX request is sent.'),
      '#default_value' => $settings['min_term_length'],
      '#size' => 20,
    );

  }

  return $form;
}

/**
 * Helper function for chosen_add_chosen_select().
 */
function _chosenselect_add_chosen($element, $form_state) {
  if (!empty($element['#width'])) {
    $element['#attributes']['style'][] = 'width:' . $element['#width'] . 'px;';
  }

  $element['#attributes']['data-placeholder'] = t($element['#placeholder']);
  if (isset($element['#autocomplete_path'])) {
    $element['#theme'] = 'hidden';
    $element['#attributes']['id'] = $element['#id'];
    if (!isset($element['#max_selection_size'])) {
      $element['#max_selection_size'] = -1;
    }
  }
  else {
    if (!empty($element['#multiple'])) {
      $element['#attributes']['multiple'] = 'multiple';
      if (!isset($element['#max_selection_size'])) {
        $element['#max_selection_size'] = -1;
      }
    }
    else {
      $element['#max_selection_size'] = 1;
    }
    $element['#theme_wrappers'] = array('form_element');
  }

  if ($element['#type'] == 'chosenselect') {
    chosenselect_attach_chosen('#' . $element['#id'], $element);
  }

  return $element;
}

/**
 * Attaches chosen to the select elements.
 */
function chosenselect_attach_chosen($selector, &$element) {
	if (file_exists(libraries_get_path('select2') . '/select2.js')) {
	  $selector_arr = explode(",", $selector);

	  $selectors['selectors'] = $selector_arr;

	  if (!empty($element['#autocomplete_path'])) {
	    $selectors['autocomplete'] = $element['#autocomplete_path'];
	    $selectors['min_term_length'] = $element['#min_term_length'];
	    $selectors['placeholder'] = t($element['#placeholder']);
	    $selectors['sortable'] = $element['#sortable'];

	    if (!empty($element['#free_tagging']) && !empty($element['#add_new_tag'])) {
	      $selectors['free_tagging'] = 2;
	    }
	    elseif (!empty($element['#free_tagging']) && empty($element['#add_new_tag'])) {
	      $selectors['free_tagging'] = 1;
	    }
	    else {
	      $selectors['free_tagging'] = 0;
	    }
	  }
	  else {
	    $selectors['autocomplete'] = FALSE;
	  }

	  $name = explode('[', $element['#name']);
	  $field = field_info_field($name[0]);
	  if (!empty($field)) {
	    $selectors['max_selection_size'] = $field['cardinality'];
	  }
	  else {
	    $selectors['max_selection_size'] = $element['#max_selection_size'];
	  }

	  $element['#attached'] = array(
	    'js' => array(
	      drupal_get_path('module', 'chosenselect') . '/chosen.attach.js',
	      libraries_get_path('select2') . '/select2.js',
	      array(
	        'type' => 'setting',
	        'data' => array('chosen' => array('attachSelectors' => array($selectors))),
	      ),
	    ),
	    'css' => array(
	      libraries_get_path('select2') . '/select2.css',
	    ),
	  );
	  if (!empty($element['#sortable'])) {
	    $element['#attached']['library'][] = array('system', 'ui.core');
	    $element['#attached']['library'][] = array('system', 'ui.widget');
	    $element['#attached']['library'][] = array('system', 'ui.mouse');
	    $element['#attached']['library'][] = array('system', 'ui.sortable');
	  }

	  if (!empty($element['#block'])) {
	    $element['#attributes']['class'][] = 'chzn-item-block';
	  }
	  $element['#attached']['css'][] = drupal_get_path('module', 'chosenselect') . '/css/chosenselect_block.css';
	} else {
	  drupal_set_message('File select2.js is not found. You should include select2 library', 'error');
	}
}

/**
 * Helper function for chosen_field_widget_form().
 */
function _chosenselect_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'chosen_term_widget') {
    $tags = array();
    foreach ($items as $item) {
      $tags[$item['tid']] = isset($item['taxonomy_term']) ? $item['taxonomy_term'] : taxonomy_term_load($item['tid']);
    }

    if (isset($element['#width'])) {
      $instance['widget']['settings']['width'] = $element['#width'];
    }

    $element += array(
      '#type' => 'chosenselect',
      '#default_value' => taxonomy_implode_tags($tags),
      '#element_validate' => array('taxonomy_autocomplete_validate'),
    );

    foreach ($instance['widget']['settings'] as $name => $value) {
      $element['#' . $name] = $value;
    }
    $element['#autocomplete_path'] = $instance['widget']['settings']['autocomplete_path'] . '/' . $field['field_name'];

  }
  if ($instance['widget']['type'] == 'chosen_select_widget') {
    // Abstract over the actual field columns, to allow different field types to
    // reuse those widgets.
    $value_key = key($field['columns']);

    $type = str_replace('options_', '', $instance['widget']['type']);
    $multiple = $field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
    $required = $element['#required'];
    $has_value = isset($items[0][$value_key]);
    $properties = _chosenselect_properties($type, $multiple, $required, $has_value);

    $entity_type = $element['#entity_type'];
    $entity = $element['#entity'];

    // Prepare the list of options.
    $options = _chosenselect_get_options($field, $instance, $properties, $entity_type, $entity);

    // Put current field values in shape.
    $default_value = _options_storage_to_form($items, $options, $value_key, $properties);

    $element += array(
      '#type' => 'chosenselect',
      '#default_value' => $default_value,
      '#value_key' => $value_key,
      '#properties' => $properties,
      '#multiple' => $multiple && count($options) > 1,
      '#options' => $options,
      '#element_validate' => array('chosenselect_field_widget_validate'),
    );

    foreach ($instance['widget']['settings'] as $name => $value) {
      $element['#' . $name] = $value;
    }
  }
  return $element;
}

/**
 * Form element validation handler for options element.
 */
function chosenselect_field_widget_validate($element, &$form_state) {
  if ($element['#required'] && $element['#value'] == '_none') {
    form_error($element, t('!name field is required.', array('!name' => filter_xss($element['#title']))));
  }
  // Transpose selections from field => delta to delta => field, turning
  // multiple selected options into multiple parent elements.
  $items = _options_form_to_storage($element);

  $name = explode('[', $element['#name']);
  if (!empty($form_state['values'][$name[0]]['und']) && !isset($_POST[$name[0]])) {
    $items = array();
  }
  form_set_value($element, $items, $form_state);
}

/**
 * Describes the preparation steps required by each widget.
 */
function _chosenselect_properties($type, $multiple, $required, $has_value) {
  $base = array(
    'filter_xss' => FALSE,
    'strip_tags' => FALSE,
    'empty_option' => FALSE,
    'optgroups' => FALSE,
  );

  $properties = array();

  $properties = array(
    // Select boxes do not support any HTML tag.
    'strip_tags' => TRUE,
    'optgroups' => TRUE,
  );
  if ($multiple) {
    // Multiple select: add a 'none' option for non-required fields.
    if (!$required) {
      $properties['empty_option'] = 'option_none';
    }
  }
  else {
    // Single select: add a 'none' option for non-required fields,
    // and a 'select a value' option for required fields that do not come
    // with a value selected.
    if (!$required) {
      $properties['empty_option'] = 'option_none';
    }
    elseif (!$has_value) {
      $properties['empty_option'] = 'option_select';
    }
  }

  return $properties + $base;
}

/**
 * Collects the options for a field.
 */
function _chosenselect_get_options($field, $instance, $properties, $entity_type, $entity) {
  // Get the list of options.
  $options = (array) module_invoke($field['module'], 'options_list', $field, $instance, $entity_type, $entity);

  // Sanitize the options.
  _options_prepare_options($options, $properties);

  if (!$properties['optgroups']) {
    $options = options_array_flatten($options);
  }

  return $options;
}

/**
 * Page callback: Outputs JSON for taxonomy autocomplete suggestions.
 */
function chosenselect_autocomplete($field_name, $free_tagging) {
  $t = array();

  $tags_typed = filter_xss($_GET['term']);

  // If the request has a '/' in the search text, then the menu system will
  // have. Split it into multiple arguments, recover the intended $tags_typed.
  $field = field_info_field($field_name);
  // Make sure the field exists and is a taxonomy field.
  if (!($field = field_info_field($field_name)) || $field['type'] !== 'taxonomy_term_reference') {
    // Error string. The JavaScript handler will realize this is not JSON and
    // will display it as debugging information.
    print t('Taxonomy field @field_name not found.', array('@field_name' => $field_name));
    exit;
  }

  // The user enters a comma-separated list of tags. We only autocomplete
  // the last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  $matches = array();
  if ($tag_last != '') {

    // Part of the criteria for the query come from the field's own settings.
    $vids = array();
    $vocabularies = taxonomy_vocabulary_get_names();
    foreach ($field['settings']['allowed_values'] as $tree) {
      $vids[] = $vocabularies[$tree['vocabulary']]->vid;
    }

    $query = db_select('taxonomy_term_data', 't');
    $query->addTag('translatable');
    $query->addTag('term_access');

    // Do not select already entered terms.
    if (!empty($tags_typed)) {
      $query->condition('t.name', $tags_typed, 'NOT IN');
    }
    // Select rows that match by term name.
    $tags_return = $query
      ->fields('t', array('tid', 'name'))
      ->condition('t.vid', $vids)
      ->condition('t.name', '%' . db_like($tag_last) . '%', 'LIKE')
      ->range(0, 10)
      ->execute()
      ->fetchAllKeyed();

    $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';

    $term_matches = array();
    foreach ($tags_return as $tid => $name) {
      $n = $name;
      // Term names containing commas or quotes must be wrapped in quotes.
      if (strpos($name, ',') !== FALSE || strpos($name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name) . '"';
      }
      $term_matches[$prefix . $n] = check_plain($name);
    }

    if ($free_tagging == 1) {
      $flag = 0;
      foreach ($term_matches as $name) {
        if (strtolower($name) == strtolower($tag_last)) {
          $flag = 1;
        }
      }
      if ($flag == 0) {
        $term_matches['new_' . $tag_last] = $tag_last;
      }
    }
    if (($free_tagging == 2) && empty($term_matches)) {
      $term_matches['new_' . $tag_last] = $tag_last;
    }
  }

  foreach ($term_matches as $key => $val) {
    $t[] = array('id' => $key, 'text' => $val);
  }
  drupal_json_output($t);
}
