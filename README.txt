Readme file for the Chosen Select module for Drupal
---------------------------------------------

This module provides form element and field widget based on
Select2 jQuery plugin (http://ivaynberg.github.com/select2).

Installation
------------

1. Extract the plugin archive and rename the folder to "select2"
in the following (recommended) location: sites/all/libraries

   So the actual plugin can be found at:
   sites/all/libraries/select2/select2.js

2. Go to "Modules" and enable the module.
-----------

Chosen Select form element could be flexibly used on any form.
You should specify form element type in 'chosenselect'. Other properties of
the element are pointed as select form element. You should specify
property '#placeholder'(help text that is displayed in the empty field) if
you are using multiple mode.
For example:
$form['color'] = array(
  '#title' => t('Select color'),
  '#type'    => 'chosenselect',
  '#multiple' => TRUE,
  '#options' => array(
    0 => t('Red'),
    1 => t('Yellow'),
    2 => t('Black'),
    3 => t('Blue'),
    4 => t('Green'),
  ),
  '#placeholder' => t('Search for a color'),
  '#description' => t('Select one of the colors.'),
);

A field widget for all kinds of lists. It can be used for these types of
fields: List (integer), List (float), List (text), Term reference.
