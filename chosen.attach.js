(function ($) {
  Drupal.behaviors.chosenAttach = {
    attach: function(context, settings) {
      var selectorArray = settings.chosen.attachSelectors;
      var minWidth = Drupal.settings.chosen.minimum_width;
      var options = {};
      options.search_contains = Drupal.settings.chosen.search_contains;

      // Attach library to all selectors.
      $.each(settings.chosen.attachSelectors, function(index, elementSettings) {
        // Select mode (no autcompletion feature). Select element is used.
        if (elementSettings.autocomplete == false) {
          $.each(elementSettings.selectors, function(index, selector) {
            $(selector, context).once('chosen', function() {
              $(this).css({
                width : ($(this).width() < minWidth) ? minWidth : $(this).width()
              }).select2({
                maximumSelectionSize: elementSettings.max_selection_size
              });
            });
          });
        // Autcompletion mode. Hidden element is used.
        } else {
          var url = window.location.protocol + "//" + window.location.host;
          $.each(elementSettings.selectors, function(index, selector) {
            $(selector).select2({
              tags: [],
              initSelection : function (element, callback) {
                var data = [];
                $(element.val().split(", ")).each(function () {
                  data.push({id: this, text: this});
                });
                callback(data);
              },
              createSearchChoice: function(term, data) {
                if ($(data).filter(function() {return this.text.localeCompare(term) === 0; }).length === 0) {return {id:term, text:term}; }
              },
              minimumInputLength: elementSettings.min_term_length,
              maximumSelectionSize: elementSettings.max_selection_size,
              ajax: {
                url: url + '/' + elementSettings.autocomplete + '/' + elementSettings.free_tagging,
                dataType: 'json',
                data: function (term, page) {
                  return {term: term};
                },
                results: function (data, page) {
                  return {results: data};
                }
              }
            });
            // Sorting is enabled.
            if (elementSettings.sortable == 1) {
              var element = $(selector);
              element.select2("container").find("ul.select2-choices").sortable({
                containment: 'parent',
                start: function() { element.select2("onSortStart"); },
                update: function() { element.select2("onSortEnd"); }
              });
            }
          });
        }
      });
    }
  }
})(jQuery);
